class DateHelper {

    constructor() {
        throw new Error('DateHelper não pode ser instanciada.');
    }

    static converterDataParaTexto(data) {
        return `${data.getDate()}/${data.getMonth() + 1}/${data.getFullYear()}`;
    }

    static converterTextoParaData(texto) {
        if (!/\d{4}-\d{2}-\d{2}/.test(texto)) {
            throw new Error('Data deve estar no formato aaaa-mm-dd.');
        }
        return new Date(...texto.split('-').map((item, index) => {
            let valor = item;
            if (index == 1) {
                valor -= 1;
            }
            return valor;
        }));
    }
}