class NegociacaoController {

    constructor() {
        const qs = document.querySelector.bind(document);
        this._inputData = qs('#data');
        this._inputQuantidade = qs('#quantidade');
        this._inputValor = qs('#valor');
        this._listaNegociacoes = new Bind(
            new ListaNegociacoes(),
            new NegociacoesView(qs('#negociacoesView')),
            'adicionar', 'limpar'
        );
        this._mensagem = new Bind(
            new Mensagem(),
            new MensagemView(qs('#mensagemView')),
            'texto'
        );
    }
    
    adicionar(event) {
        event.preventDefault();
        this._listaNegociacoes.adicionar(this._criarNegociacao());
        this._mensagem.texto = 'Negociação adicionada com sucesso.';
        this._limparFormulario();
    }

    importarNegociacoes() {
        const negociacaoService = new NegociacaoService();
        negociacaoService.obterTodasNegociacoes()
            .then(negociacoes => {
                negociacoes.forEach(negociacao => this._listaNegociacoes.adicionar(negociacao));
                this._mensagem.texto = 'Negociações obtidas com sucesso';
            })
            .catch(error => 
                this._mensagem.texto = error
            );
    }

    limpar() {
        this._listaNegociacoes.limpar();
        this._mensagem.texto = 'Negociações apagadas com sucesso.';
    }

    _criarNegociacao() {
        return new Negociacao(
            DateHelper.converterTextoParaData(this._inputData.value),
            this._inputQuantidade.value,
            this._inputValor.value
        );
    }

    _limparFormulario() {
        this._inputData.value = '';
        this._inputQuantidade.value = 1;
        this._inputValor.value = 0.0;
        this._inputData.focus();
    }
}