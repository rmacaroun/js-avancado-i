class ListaNegociacoes {

    constructor() {
        this._negociacoes = new Array();
    }

    get negociacoes() {
        return new Array().concat(this._negociacoes);
    }
    
    adicionar(negociacao) {
        this._negociacoes.push(negociacao);
    }
    
    limpar() {
        this._negociacoes = new Array();
    }

    get volumeTotal() {
        return this._negociacoes.reduce((total, n) => total + n.volume, 0,0);
    }
}