class ProxyFactory {

    static create(objeto, props, acao) {
        return new Proxy(objeto, {
            get(target, prop, handler) {
                if (props.includes(prop) && ProxyFactory._isFuncao(target[prop])) {
                    return function() {
                        console.log(`Interceptando ${prop}.`);
                        Reflect.apply(target[prop], target, arguments);
                        return acao(target);
                    }
                }
                return Reflect.get(target, prop, handler);
            },
            set(target, prop, value, handler) {
                if (props.includes(prop)) {
                    target[prop] = value;
                    acao(target);
                }
                return Reflect.set(target, prop, value, handler);
            }
        });
    }

    static _isFuncao(func) {
        return typeof(func) == typeof(Function);
    }
}