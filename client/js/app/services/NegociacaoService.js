class NegociacaoService {

    constructor() {
        this._httpService = new HttpService();
    }

    obterTodasNegociacoes() {
        return Promise.all(
            [
                this.obterNegociacoesSemana(),
                this.obterNegociacoesSemanaAnterior(),
                this.obterNegociacoesSemanaRetrasada()
            ]
        )
        .then(negociacoes => 
            negociacoes.reduce((arrayAchatado, array) => arrayAchatado.concat(array), [])
        )
        .catch(error => {
            throw new Error(error)
        });
    }

    obterNegociacoesSemana() {
        return this.obterNegociacoes('semana', 'Não foi possível obter as negociações da semana.');
    }

    obterNegociacoesSemanaAnterior() {
        return this.obterNegociacoes('anterior', 'Não foi possível obter as negociações da semana anterior.');
    }

    obterNegociacoesSemanaRetrasada() {
        return this.obterNegociacoes('retrasada', 'Não foi possível obter as negociações da semana retrasada.');
    }

  obterNegociacoes(periodo, msgErro) {
    return this._httpService
        .get('negociacoes/'.concat(periodo))
        .then(negociacoes => {
            return negociacoes.map(objeto => 
                new Negociacao(new Date(objeto.data), objeto.quantidade, objeto.valor)
            )
        })
        .catch(error => {
            console.log(error);
            throw new Error(msgErro);
        });
  }
}